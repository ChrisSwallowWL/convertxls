# README #

### What is this repository for? ###

This simple little utility will convert an old .xls spreadsheet file to .xlsx.

It doesn't do some formatting or functionality though so it's fairly basic

It uses the xls2xlsx library from here:  https://pypi.org/project/xls2xlsx/

### How do I get set up? ###

* Install Python if you don't have it already (https://www.python.org/downloads/)
* Install the xls2xlsx library using:
  * _pip install -r requirements.txt_
* Run the command:
  * python convert.py \<filename and path\>
