#!/usr/bin/env python

""" Name: ConvertXLS.py
    Description: Convert an XLS file to an XLSX file to workaround the corporate restriction on opening XLS files
    Author: Chris Swallow
    Date: 23/06/2021
"""

import getopt
import sys

from xls2xlsx import XLS2XLSX


def usage():
    print('Usage: ConvertXLS.py -f <name of file to convert>')


def main(argv):
    # Process the arguments to retrieve the credentials
    xls_file = ''

    try:
        opts, args = getopt.getopt(argv, "f:",
                                   ["xls_file="])
    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:

        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-f", "--xls_file"):
            xls_file = arg

    # Error if a filename was not supplied
    if xls_file == '':
        print("A filename (including path) must be supplied.")
        usage()
        sys.exit(2)

    # Display a message of the name of the file being loaded
    print("Converting the XLS file %s" % xls_file)

    # Set the new file name (assumes .xls extension)
    xlsx_file = xls_file + "x"

    # Convert the file
    x2x = XLS2XLSX(xls_file)
    x2x.to_xlsx(xlsx_file)

    print('File %s converted and saved as %s' % (xls_file, xlsx_file))


if __name__ == "__main__":
    main(sys.argv[1:])
